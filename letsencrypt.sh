#!/bin/bash

mkdir -p ./letsencrypt/{webroot,archive,accounts}

docker run -it --rm --name letsencrypt -v $PWD/letsencrypt/webroot:/var/webroot -v $PWD/letsencrypt/archive:/etc/letsencrypt/archive -v $PWD/letsencrypt/accounts:/etc/letsencrypt/accounts certbot/certbot:latest certonly -n --allow-subset-of-names --webroot --webroot-path=/var/webroot/ --agree-tos --register-unsafely-without-email -d 1.kosenko.pp.ua -d 2.kosenko.pp.ua -d www.1.kosenko.pp.ua -d www.2.kosenko.pp.ua

mv ./letsencrypt/archive/1.kosenko.pp.ua/* ./nginx/certs/

docker exec -it nginx nginx -s reload
